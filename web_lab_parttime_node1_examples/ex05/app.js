var express = require('express');
var app = express();
app.set('port', process.env.PORT || 3000);

//creating route for home page
app.get('/', function(req, res){
    res.type('text/html');
    res.sendFile(__dirname + '/public/animatedbook.html');
});
//serving all files in public to the client
app.use(express.static(__dirname + '/public'));

//listening to port 3000
app.listen(app.get('port'), function() {
    console.log('express started listening on localhost:' + app.get('port'));
});