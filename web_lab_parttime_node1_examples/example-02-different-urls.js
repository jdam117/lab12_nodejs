var http = require('http');
http.createServer(function (req, res) {
    var lookup = path.basename(decodeURI(req.url));

    if (lookup == 'helloAgain') {
        res.writeHead(200,
            { 'Content-Type': 'text/plain' });
        res.end('Hello again!');
    } else {
        res.writeHead(200,
            { 'Content-Type': 'text/plain' });
        res.end('Hello world!');
    }
}).listen(3000);
