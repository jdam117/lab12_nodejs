// Require the express library
var express = require('express');

// Setup Express server
var app = express();

// When this server receives a GET request to "/", it should send back the ./public/about.html file.
app.get('/', function (req, res) {
    res.sendFile(__dirname + "/public/about.html");
});

// When this server receives a GET request to "/hello", it should send back the given HTML code.
app.get('/hello', function (req, res) {
    res.send("<!DOCTYPE html><html>" +
        "<head>" +
        "<title>Exercise 02</title>" +
        "</head>" +
        "<body>" +
        "<p>About me: I am <strong>Awesome!</strong></p>" +
        "</body></html>");
});

// Allow this server to server any file in the "public" folder. For example, if the server receives a
// request for /images/Charmander.png, it will send back ./public/images/Charmander.png.
app.use(express.static(__dirname + '/public'));

// Start the server running, listening on port 3000.
// When the server has started, the given function will be called to let us know.
app.listen(3000, function () {
    console.log('Simple web app listening at http://localhost:3000;');
});
