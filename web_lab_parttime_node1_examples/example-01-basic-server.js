// "require" the http library in order to function
var http = require('http');

// Create a server listening on port 3000. When that server receives any
// HTTP request, the given function will be called.
http.createServer(function (req, res) {

	// Write the Content-Type header, which tells the browser to interpret the incoming response
	// as plain text.
	res.writeHead(200, { 'Content-Type': 'html' });

	// Write some text out to the browser
	res.write('<h1>This is my first server!</h1>');

	// Finish up with one more line of text
	res.end('Hello world!');

}).listen(3000);
