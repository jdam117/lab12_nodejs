var http = require('http');
var path = require('path');

http.createServer(function (req, res) {
    var lookup = path.basename(decodeURI(req.url));

    if (lookup == "") {
        res.writeHead(200, { "Content-Type": "text/html" })
        res.write("<!DOCTYPE html> <html>");
        res.write("<head>");
        res.write("<title>Exercise02</title>");
        res.write("</head>");
        res.write("<body>");
        res.write("<p>my page");
        res.write("</body></html>");
        res.end();
    }
    if (lookup == "about") {
        res.writeHead(200, { "Content-Type": "text/html" })
        res.write("<!DOCTYPE html> <html>");
        res.write("<head>");
        res.write("<title>Exercise02 - about</title>");
        res.write("</head>");
        res.write("<body>");
        res.write("<p>some information about me");
        res.write("</body></html>");
        res.end();
    }
    if (lookup == "contact") {
        res.writeHead(200, { "Content-Type": "text/html" })
        res.write("<!DOCTYPE html> <html>");
        res.write("<head>");
        res.write("<title>Exercise02 - contact </title>");
        res.write("</head>");
        res.write("<body>");
        res.write("<p>some contact information about me");
        res.write("</body></html>");
        res.end();

    }
    if (!res.finished) {
        res.writeHead(404, { 'Content-Type': 'text/html' });
        res.end('Page Not Found')
    }



}).listen(3000);